<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function daftar() {
        return view("pages.form");
    }

    public function register(Request $request) {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('pages.home', ["namaDepan" => $namaDepan, "namaBelakang" =>$namaBelakang]);
    }
}
