@extends('layouts.master')

@section('judul', 'Form')

@section('content')
<form action="/signup" method="POST">
    @csrf
    <label for="">Nama Depan</label> <br>
    <input type="text" name="fname"> <br> <br>
    <label for="">Nama Belakang</label> <br>
    <input type="text" name="lname" required> <br> <br>

    <button type="submit">Send</button>
</form>
@endsection