<!DOCTYPE html>
<html>
<head>
    <title>Register Page</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf <!-- CSRF Token for form security -->
        <p>First Name:</p>
        <p><input name="firstName" type="text" /></p>
        <p>Last Name:</p>
        <p><input name="lastName" type="text" /></p>
        <p>Gender:</p>
        <p>
            <input name="gender" type="radio" value="male" /> Male<br />
            <input name="gender" type="radio" value="female" /> Female<br />
            <input name="gender" type="radio" value="other" /> Other
        </p>
        <p>Nationality:</p>
        <select name="nationality">
            <option value="">-- select one --</option>
            <option value="indonesian">Indonesian</option>
            <option value="english">English</option>
            <!-- Tambahkan opsi lainnya sesuai kebutuhan -->
        </select>
        <p>Language Spoken:</p>
        <p>
            <input name="language[]" type="checkbox" value="Bahasa Indonesia" /> Bahasa Indonesia<br />
            <input name="language[]" type="checkbox" value="English" /> English<br />
            <input name="language[]" type="checkbox" value="Other" /> Other
        </p>
        <p>Bio:</p>
        <textarea cols="50" name="bio" rows="4"></textarea>
        <p><input type="submit" value="Submit" /></p>
    </form>
</body>
</html>
